export const options = [
    {
        "type": "wheels",
        "name": "18 Aero",
        'default': true,
        "code": "$W38B",
        "asset": "/assets/img/wheels/opt-wheels-18pinwheel.png",
        'activeRanges': [
            'standard_range',
            'standard_range_plus',
            'long_range_rwd',
            'long_range_awd',
            'long_range_performance'
        ],
    },{
        "type": "wheels",
        "name": "19 Sport",
        "code": "$W39B",
        "asset": "/assets/img/wheels/opt-wheels-19stilette.png",
        'activeRanges': [
            'standard_range',
            'standard_range_plus',
            'long_range_rwd',
            'long_range_awd',
            'long_range_performance'
        ],
    },{
        "type": "wheels",
        "name": "20 Performance",
        "code": "$W32B",
        "asset": "/assets/img/wheels/opt-wheels-20inch.png",
        'activeRanges': [
            'standard_range',
            'standard_range_plus',
            'long_range_rwd',
            'long_range_awd',
            'long_range_performance'
        ],
    },{
        "type": "color",
        "name": "Solid Black",
        'default': true,
        "code": "$PBSB",
        'asset': '/assets/img/colors/ui_swat_col_pbsb.png',
        'activeRanges': [
            'standard_range', 
            'standard_range_plus', 
            'long_range_rwd', 
            'long_range_awd', 
            'long_range_performance'
        ],
    },{
        "type": "color",
        "name": "Midnight Silver Metallic",
        "code": "$PMNG",
        'asset': '/assets/img/colors/ui_swat_col_pmng.png',
        'activeRanges': [
            'standard_range', 
            'standard_range_plus', 
            'long_range_rwd', 
            'long_range_awd', 
            'long_range_performance'
        ],
    },{
        "type": "color",
        "name": "Deep Blue Metallic",
        "code": "$PPSB",
        'asset': '/assets/img/colors/ui_swat_col_ppsb.png',
        'activeRanges': [
            'standard_range', 
            'standard_range_plus', 
            'long_range_rwd', 
            'long_range_awd', 
            'long_range_performance'
        ],
    },{
        "type": "color",
        "name": "Obsidian Black",
        "code": "$PMBL",
        'asset': '/assets/img/colors/ui_swat_col_pmbl.png',
        'activeRanges': [
            'standard_range', 
            'standard_range_plus', 
            'long_range_rwd', 
            'long_range_awd', 
            'long_range_performance'
        ],
    },{
        "type": "color",
        "name": "Midnight Silver",
        "code": "$PMSS",
        'asset': '/assets/img/colors/ui_swat_col_pmss.png',
        'activeRanges': [
            'standard_range', 
            'standard_range_plus', 
            'long_range_rwd', 
            'long_range_awd', 
            'long_range_performance'
        ],
    },{
        "type": "color",
        "name": "Pearl White Multi-Coat",
        "code": "$PPSW",
        'asset': '/assets/img/colors/ui_swat_col_ppsw.png',
        'activeRanges': [
            'standard_range', 
            'standard_range_plus', 
            'long_range_rwd', 
            'long_range_awd', 
            'long_range_performance'
        ],
    },{
        "type": "color",
        "name": "Red Multi-Coat",
        "code": "$PPMR",
        'asset': '/assets/img/colors/ui_swat_col_ppmr.png',
        'activeRanges': [
            'standard_range', 
            'standard_range_plus', 
            'long_range_rwd', 
            'long_range_awd', 
            'long_range_performance'
        ],
        
    },{
        "type": "interior",
        "name": "Black",
        "code": "$IN3PB",
        'default': true,
        'asset': '/assets/img/interiors/black.png',
        'activeRanges': [
            'standard_range', 
            'standard_range_plus', 
            'long_range_rwd', 
            'long_range_awd', 
            'long_range_performance'
        ],
    },{
        "type": "interior",
        "name": "Black + White",
        "code": "$IN3PW",
        'asset': '/assets/img/interiors/white.png',
        'activeRanges': [
            'standard_range', 
            'standard_range_plus', 
            'long_range_rwd', 
            'long_range_awd', 
            'long_range_performance'
        ],
    },
]